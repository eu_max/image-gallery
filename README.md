# Resource uploader demo

## Prerequisites
- Install the Java 11
- Install the Apache Maven (at least `3.6.3` version)
- Install the Git
- Install the Docker and Docker Compose

## How to start
### Clone this repo
Use ssh
```
git clone git@bitbucket.org:eu_max/image-gallery.git
```
or https
```
git clone https://eu_max@bitbucket.org/eu_max/image-gallery.git
```
### Checkout the _dev_ branch
```
cd image-gallery
git checkout dev
git pull
```
### Build and run the project
Execute bash script
```
docker-compose up -d
mvn clean package
java -jar target/image-gallery-0.0.1-SNAPSHOT-application.jar
``` 
### Execute next GET requests for functionality checking
```
 http://localhost:8081/images
 http://localhost:8081/images/{id}
 http://localhost:8081/pictures/{path}
 http://localhost:8081/search/{searchTerm} 
```

### Technologies stack
- Java 11
- Spring Boot
- Elasticsearch
- OpenFeign
- Spring Cache(default in-memory)
