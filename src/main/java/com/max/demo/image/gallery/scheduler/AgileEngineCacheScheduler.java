package com.max.demo.image.gallery.scheduler;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.max.demo.image.gallery.api.dto.PageDto;
import com.max.demo.image.gallery.service.AgileEngineImageService;
import com.max.demo.image.gallery.service.PictureService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class AgileEngineCacheScheduler {

    private final PictureService pictureService;
    private final AgileEngineImageService agileEngineImageService;

    @Scheduled(fixedDelayString = "${agile-engine.cache-reload-duration}")
    @CacheEvict(cacheNames = "agileEngineImage", allEntries = true, beforeInvocation = true)
    public void reloadAgileEngineApiCache() {
        log.info("agile-engine cache reloading is started");
        reloadCache();
        log.info("agile-engine cache reloading is finished");
    }

    private void reloadCache() {
        pictureService.deleteAll();
        var hasMore = true;
        int page = 1;
        while (hasMore) {
            var pageDto = agileEngineImageService.getImages(page++);
            hasMore = Optional.ofNullable(pageDto)
                    .map(PageDto::getHasMore)
                    .orElse(false);

            Optional.ofNullable(pageDto)
                    .map(this::fetchPicturesById)
                    .ifPresent(pictureService::create);
        }
    }

    private List<Map<String, Object>> fetchPicturesById(PageDto<Map<String, Object>> response) {
        return response.getPictures()
                .stream()
                .map(map -> map.get("id"))
                .map(String.class::cast)
                .map(agileEngineImageService::getImageById)
                .collect(Collectors.toList());
    }
}
