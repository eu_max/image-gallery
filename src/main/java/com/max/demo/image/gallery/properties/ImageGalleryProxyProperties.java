package com.max.demo.image.gallery.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "image-gallery-proxy")
public class ImageGalleryProxyProperties {

    private String host;
}
