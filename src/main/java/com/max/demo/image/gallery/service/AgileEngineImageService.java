package com.max.demo.image.gallery.service;

import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import com.max.demo.image.gallery.api.dto.PageDto;

public interface AgileEngineImageService {

    PageDto<Map<String, Object>> getImages(Integer page);

    Map<String, Object> getImageById(String id);

    ResponseEntity<Resource> downloadPicture(String path);
}
