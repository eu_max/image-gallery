package com.max.demo.image.gallery.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.max.demo.image.gallery.api.dto.PageDto;
import com.max.demo.image.gallery.feign.config.FeignConfiguration;

@FeignClient(name = "agileEngineApi", configuration = FeignConfiguration.class, url = "${agile-engine.host}")
public interface AgileEngineFeignClient {

    @GetMapping("/images")
    ResponseEntity<PageDto<Map<String, Object>>> getImages(@RequestParam Integer page);

    @GetMapping("/images/{id}")
    ResponseEntity<Map<String, Object>> getImageById(@PathVariable String id);

    @GetMapping("/pictures/**")
    ResponseEntity<Resource> downloadPicture(String path);
}
