package com.max.demo.image.gallery.feign.config;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import com.max.demo.image.gallery.properties.AgileEngineProperties;

import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.Retryer;
import lombok.RequiredArgsConstructor;

@Configuration
@EnableFeignClients(basePackages = "com.max.demo.image.gallery.feign")
public class FeignConfiguration {

    @Bean
    public Retryer retryer() {
        return new Retryer.Default();
    }

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public RequestInterceptor requestInterceptor(AgileEngineProperties agileEngineProperties) {
        return new Oauth2AgileEngineInterceptor(agileEngineProperties);
    }

    @RequiredArgsConstructor
    private static class Oauth2AgileEngineInterceptor implements RequestInterceptor {

        private final RestTemplate restTemplate = new RestTemplate();
        private final AgileEngineProperties agileEngineProperties;

        @Override
        public void apply(RequestTemplate template) {
            template.header("Authorization", getToken());
        }

        private String getToken() {
            return Optional.of(restTemplate.postForEntity(agileEngineProperties.getHost() + "/auth",
                    Collections.singletonMap("apiKey", agileEngineProperties.getApiKey()),
                    Map.class))
                    .map(HttpEntity::getBody)
                    .map(map -> map.get("token"))
                    .map(String.class::cast)
                    .orElse(null);
        }
    }
}

