package com.max.demo.image.gallery.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.max.demo.image.gallery.api.ImageGalleryApi;
import com.max.demo.image.gallery.service.AgileEngineImageService;
import com.max.demo.image.gallery.service.PictureService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ImageGalleryController implements ImageGalleryApi {

    private final PictureService pictureService;
    private final AgileEngineImageService agileEngineImageService;

    @Override
    public ResponseEntity<?> getPictures(Integer page) {
        return ResponseEntity.ok(agileEngineImageService.getImages(page));
    }

    @Override
    public ResponseEntity<?> getPictureById(String id) {
        return ResponseEntity.ok(agileEngineImageService.getImageById(id));
    }

    @Override
    public ResponseEntity<Resource> downloadPicture(HttpServletRequest request) {
        return agileEngineImageService.downloadPicture(request.getServletPath());
    }

    @Override
    public ResponseEntity<?> searchPictures(String searchTerm) {
        return ResponseEntity.ok(pictureService.search(searchTerm));
    }
}
