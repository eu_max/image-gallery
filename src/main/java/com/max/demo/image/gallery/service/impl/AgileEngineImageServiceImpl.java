package com.max.demo.image.gallery.service.impl;

import java.net.URLConnection;
import java.util.Map;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.max.demo.image.gallery.api.dto.PageDto;
import com.max.demo.image.gallery.feign.AgileEngineFeignClient;
import com.max.demo.image.gallery.properties.AgileEngineProperties;
import com.max.demo.image.gallery.properties.ImageGalleryProxyProperties;
import com.max.demo.image.gallery.service.AgileEngineImageService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Service
@RequiredArgsConstructor
public class AgileEngineImageServiceImpl implements AgileEngineImageService {

    private final AgileEngineProperties agileEngineProperties;
    private final AgileEngineFeignClient agileEngineFeignClient;
    private final ImageGalleryProxyProperties imageGalleryProxyProperties;

    @Override
    @Cacheable("agileEngineImage")
    public PageDto<Map<String, Object>> getImages(Integer page) {
        var pageDto = agileEngineFeignClient.getImages(page)
                .getBody();
        Optional.ofNullable(pageDto.getPictures())
                .ifPresent(pictures -> pictures.forEach(this::rewriteAbsoluteUrl));
        return pageDto;
    }

    @Override
    @Cacheable("agileEngineImage")
    public Map<String, Object> getImageById(String id) {
        var image = agileEngineFeignClient.getImageById(id)
                .getBody();
        Optional.ofNullable(image)
                .ifPresent(this::rewriteAbsoluteUrl);
        return image;
    }

    @Override
    @SneakyThrows
    @Cacheable("agileEngineImage")
    public ResponseEntity<Resource> downloadPicture(String path) {
        var urlResource = new UrlResource(agileEngineProperties.getHost() + path);
        var resource = new ByteArrayResource(StreamUtils.copyToByteArray(urlResource.getInputStream()));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, URLConnection.guessContentTypeFromName(path))
                .body(resource);
    }

    private void rewriteAbsoluteUrl(Map<String, Object> map) {
        map.entrySet()
                .stream()
                .filter(entry -> String.class.equals(entry.getValue()
                        .getClass()))
                .forEach(entry -> entry.setValue(
                        ((String) entry.getValue()).replace(
                                agileEngineProperties.getHost(), imageGalleryProxyProperties.getHost())));
    }
}
