package com.max.demo.image.gallery.api.dto;

import java.util.List;

import lombok.Data;

@Data
public class PageDto<T> {

    private List<T> pictures;
    private Integer page;
    private Integer pageCount;
    private Boolean hasMore;
}
