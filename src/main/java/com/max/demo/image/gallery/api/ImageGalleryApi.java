package com.max.demo.image.gallery.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public interface ImageGalleryApi {

    @GetMapping("/images")
    ResponseEntity<?> getPictures(@RequestParam(required = false) Integer page);

    @GetMapping("/images/{id}")
    ResponseEntity<?> getPictureById(@PathVariable String id);

    @GetMapping("/pictures/**")
    ResponseEntity<Resource> downloadPicture(HttpServletRequest request);

    @GetMapping("/search/{searchTerm}")
    ResponseEntity<?> searchPictures(@PathVariable String searchTerm);
}
