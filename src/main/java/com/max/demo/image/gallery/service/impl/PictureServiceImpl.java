package com.max.demo.image.gallery.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import com.max.demo.image.gallery.service.PictureService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Service
@RequiredArgsConstructor
public class PictureServiceImpl implements PictureService {

    public static final String PICTURES_INDEX = "pictures";

    private final RestHighLevelClient restHighLevelClient;

    @Override
    @SneakyThrows
    public List<Map<String, Object>> search(String searchQuery) {
        var response = restHighLevelClient.search(new SearchRequest(PICTURES_INDEX)
                        .source(SearchSourceBuilder.searchSource()
                                .query(new SimpleQueryStringBuilder(searchQuery))),
                RequestOptions.DEFAULT);
        return Arrays.stream(response.getHits()
                .getHits())
                .map(SearchHit::getSourceAsMap)
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void create(List<Map<String, Object>> pictures) {
        var bulkRequest = new BulkRequest(PICTURES_INDEX);
        pictures.stream()
                .map(map -> new IndexRequest().source(map))
                .forEach(bulkRequest::add);
        restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

    @Override
    @SneakyThrows
    public void deleteAll() {
        if (restHighLevelClient.indices()
                .exists(new GetIndexRequest(PICTURES_INDEX), RequestOptions.DEFAULT)) {
            restHighLevelClient.deleteByQuery(new DeleteByQueryRequest(PICTURES_INDEX).setQuery(new MatchAllQueryBuilder()),
                    RequestOptions.DEFAULT);
        }
    }
}
