package com.max.demo.image.gallery.service;

import java.util.List;
import java.util.Map;

public interface PictureService {

    List<Map<String, Object>> search(String searchQuery);

    void create(List<Map<String, Object>> pictures);

    void deleteAll();
}
